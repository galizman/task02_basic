package com.epam;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Basic {
    public static void main(String[] args) {
        int lowInterval;
        int highInteval;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the interval (for example: [1;100]:");
        lowInterval = in.nextInt();
        highInteval = in.nextInt();
        getNumbers(lowInterval, highInteval);
    }
    private static void getNumbers(int lowInterval, int highInterval){
        int sumOdd = 0;
        int sumEven = 0;
        System.out.println("Start from " + lowInterval + " to " + highInterval);
        for (int i = lowInterval; i <= highInterval; i++) {
            if (isEven(i)) {
                sumEven += i;
            } else {
                sumOdd += i;
                if (isEven(highInterval)) {
                    System.out.println("Odd: " + i + " Even: " + (highInterval - i + 1));
                } else {
                    System.out.println("Odd: " + i + " Even: " + (highInterval - i));
                }
            }
        }
        System.out.println("Summ of odd numbers: " + sumOdd);
        System.out.println("Summ of even numbers: " + sumEven);
    }
    private static boolean isEven(int number){
        return number % 2 == 0;
    }
}